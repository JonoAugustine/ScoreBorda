import Home from "./Home";
import Borda from "./Borda";
import History from "./History";

import "./page.scss";

export { Home, Borda, History };
