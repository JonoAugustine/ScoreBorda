import React from "react";
import { RoundContext } from "../../state";
import { BordaState } from "../../api/models";
import Setup from "./Setup";
import Calibration from "./Calibration";
import Scorring from "./Scorring";

import "./running.scss";
import Complete from "./Complete";

/**
 * Renders the appropriate borda screen based on the round state.
 * @param {String} state - The current state of the round
 */
const renderState = (state) => {
  switch (state) {
    case BordaState.COMPLETE:
      return <Complete />;
    case BordaState.RUNNING.CALIBRATION:
      return <Calibration />;
    case BordaState.RUNNING.SCORRING:
      return <Scorring />;
    default:
      return <Setup />;
  }
};

const Borda = () => {
  const { state } = React.useContext(RoundContext);

  return <div className="page borda">{renderState(state)}</div>;
};

export default Borda;
