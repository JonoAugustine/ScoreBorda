import React from "react";
import { withRouter } from "react-router-dom";
import { RoundContext } from "../../state";
import InputColumn from "../../components/InputColumn/InputColumn";
import { BordaState, Feature } from "../../api/models";

/**
 * TODO Allow setup to pull from previous rounds
 */
const Setup = () => {
  const {
    state,
    features,
    candidates,
    setFeatures,
    setCandidates,
    nextState,
  } = React.useContext(RoundContext);

  const confirm = () => {
    if (state === BordaState.SETUP.FEATURES && features.length < 1) {
      // TODO Toast saying features list is empty
      console.log("Feature list empty");
      return;
    }

    if (state === BordaState.SETUP.CANDIDATES) {
      setFeatures(features.map((fn) => new Feature(fn)));

      if (candidates.length < 2) {
        // TODO Toast saying candidate list is empty
        console.log("Candidate list empty");
        return;
      }
    }

    nextState();
  };

  return (
    <div className="setup">
      <React.Fragment>
        <header>
          <h2>Setup {state}</h2>
        </header>
        <section>
          <InputColumn
            name={state}
            values={state === BordaState.SETUP.FEATURES ? features : candidates}
            setValues={
              state === BordaState.SETUP.FEATURES ? setFeatures : setCandidates
            }
          />
        </section>
        <section>
          <button onClick={confirm}>Confirm</button>
        </section>
      </React.Fragment>
    </div>
  );
};

export default withRouter(Setup);
