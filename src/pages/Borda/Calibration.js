import React from "react";
import { util } from "../../api";
import { RoundContext } from "../../state";
import { Candidate } from "../../api/models";

/**
 * Feature calibration
 */
const Calibration = () => {
  const {
    features,
    candidates,
    setCandidates,
    nextState,
    iterable,
    setIterable,
  } = React.useContext(RoundContext);

  const [left, setLeft] = React.useState(null);
  const [right, setRight] = React.useState(null);

  React.useEffect(() => {
    if (iterable) nextPair();
    else setIterable(new util.BordaIterable(features));
  }, [iterable]);

  const nextPair = () => {
    const pair = iterable.step();

    if (pair) {
      setLeft(pair[0]);
      setRight(pair[1]);
    } else {
      console.log("calibration done. conforming candidates");
      // Conform candidates
      setCandidates(
        candidates.map(
          (cn) =>
            new Candidate(
              cn,
              features.map((f) => f.clone())
            )
        )
      );
      nextState();
    }
  };

  const addToLeft = () => {
    left.score++;
    nextPair();
  };

  const addToRight = () => {
    right.score++;
    nextPair();
  };

  return (
    <div className="calibration">
      <header>
        <h1>Feature Calibration</h1>
        <p>Click which feature is more important to you</p>
      </header>
      <main>
        <button onClick={addToLeft}>{left?.name}</button>
        <button onClick={addToRight}>{right?.name}</button>
      </main>
    </div>
  );
};

export default Calibration;
