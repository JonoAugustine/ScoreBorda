import React from "react";
import { RoundContext } from "../../state";
import { util } from "../../api";

/**
 * Candidate borda
 */
const Scorring = (props) => {
  const {
    features,
    candidates,
    state,
    nextState,
    iterable,
    setIterable,
  } = React.useContext(RoundContext);

  const [featureIndex, setFeatureIndex] = React.useState(0);
  const [left, setLeft] = React.useState(null);
  const [right, setRight] = React.useState(null);

  // Resets candidate iterable and advances state dependant on featureIndex
  React.useEffect(() => {
    // Step to next state if features exhausted
    if (featureIndex >= features.length) nextState();
    else setIterable(new util.BordaIterable(candidates)); // TODO Shuffle
  }, [featureIndex]);

  // Updates buttons on iterable reset
  React.useEffect(() => {
    nextPair();
  }, [iterable]);

  const nextFeature = () => setFeatureIndex(featureIndex + 1);

  /**
   * Moves to the next pair of candidates.
   * When feature scorring is complete, moves to
   * next faeture and restarts iterable
   */
  const nextPair = () => {
    // Step the candidate iteration
    const pair = iterable.step();
    // Check if the iteration is complete
    if (pair === null) {
      // If iteration is complete, step to next feature
      nextFeature();
    } else {
      // Update candidates
      setLeft(pair[0]);
      setRight(pair[1]);
    }
  };

  /** Add to left candidate feature score */
  const addToLeft = () => {
    // Add to the left
    left.features[featureIndex].score++;
    // step to next pair
    nextPair();
  };

  /** Add to right candidate feature score */
  const addToRight = () => {
    // Add to the right
    right.features[featureIndex].score++;
    // step to next pair
    nextPair();
  };

  return (
    <div className="scorring">
    <header>
    <h1>Candidate Scorring</h1>
    <p>Click on the candidate which has the best/best embodies "{features[featureIndex]?.name}"</p>
    </header>
     
      <section className="borda-button-section">
        <button className="borda-button" onClick={addToLeft}>
          {left?.name}
        </button>
        <button className="borda-button" onClick={addToRight}>
          {right?.name}
        </button>
      </section>
    </div>
  );
};

export default Scorring;
