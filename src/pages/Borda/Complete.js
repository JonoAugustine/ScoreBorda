import React from "react";
import { RoundContext } from "../../state";
import { dao } from "../../api";
import { useHistory } from "react-router-dom";

/**
 * Displays final borda results and saves results
 */
const Complete = () => {
  const { candidates, features, state } = React.useContext(RoundContext);
  const history = useHistory();

  /**
   * Save round context to db, then opens home page
   */
  const saveAndGoHome = () => {
    dao.saveScore({ candidates, features, state });
    history.push("/");
  };

  return (
    <div className="complete">
      <h1>Borda Completed</h1>
      <section>
        <ul>
          {candidates
            .map((c) => ({ name: c.name, score: c.rawScore() }))
            .sort((c1, c2) => c2.score - c1.score)
            .map(({ name, score }) => (
              <li key={name}>
                {name}: {score}
              </li>
            ))}
        </ul>
      </section>
      <button onClick={saveAndGoHome}>Continue</button>
    </div>
  );
};

export default Complete;
