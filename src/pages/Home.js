import React from "react";
import { LinkButton } from "../components";

const Home = () => {
  return (
    <div className="page home">
      <header>
        <h1>ScoreBorda</h1>
      </header>
      <main>
        <LinkButton href="/borda">Start</LinkButton>
      </main>
    </div>
  );
};

export default Home;
