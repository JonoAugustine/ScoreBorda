import React from "react";
import { dao } from "../api";

// TODO add detail cards for each round
const History = () => {
  const [scores, setScores] = React.useState(dao.loadScores() || []);

  const clear = () => {
    dao.clearScores();
    setScores([]);
  };

  console.log(scores);

  return (
    <div className="page history">
      <header>
        <h2>Your Previous Bordas</h2>
      </header>
      <main>
        <ul>
          {scores
            .map((s) => ({ ...s, time: new Date(s.time) }))
            .map((s) => (
              <li>
                {s.time.getDate()}/{s.time.getMonth()}/{s.time.getFullYear()}
              </li>
            ))}
        </ul>
        <button onClick={clear} danger>
          Clear History
        </button>
      </main>
    </div>
  );
};

export default History;
