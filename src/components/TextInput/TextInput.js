import React from "react";
import PropTypes from "prop-types";

const TextInput = (props) => {
  return (
    <div className="text-input">
      <input
        name={props.name}
        type={props?.type || props?.name || "text"}
        required
        autoComplete="off"
        ref={props.reference}
        {...props}
      />
      <label htmlFor={props.name} className="floating">
        <span className="content-name">{props.name}</span>
      </label>
    </div>
  );
};

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
};

export default TextInput;
