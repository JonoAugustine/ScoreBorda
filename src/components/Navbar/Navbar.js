import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="navbar">
      <section className="brand">
        <Link to="/">
          <h1>ScoreBorda</h1>
        </Link>
      </section>
      <section className="menu">
        <ul>
          <li>
            <a href={process.env.PUBLIC_URL + "/history"}>History</a>
          </li>
          <li>
            <a href={process.env.PUBLIC_URL + "/settings"}>Settings</a>
          </li>
        </ul>
      </section>
    </nav>
  );
};

export default Navbar;
