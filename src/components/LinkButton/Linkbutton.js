import React from "react";

const Linkbutton = (props) => {
  return (
    <a href={process.env.PUBLIC_URL + props.href} className="link-button">
      <button>{props.children}</button>
    </a>
  );
};

export default Linkbutton;
