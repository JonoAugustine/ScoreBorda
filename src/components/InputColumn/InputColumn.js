import React from "react";
import PropTypes from "prop-types";
import TextInput from "../TextInput";

const InputColumn = ({ name, values, setValues }) => {
  const nameInput = React.useRef();

  React.useEffect(() => {
    nameInput.current && nameInput.current.focus();
  }, [values]);

  /** Add the value from the form submit to the state */
  const addValue = (event) => {
    // Stop page refresh
    event.preventDefault();
    // Confirm value is not duplicate
    if (!values.includes(event.target[0].value)) {
      // Add value to state
      setValues([...values, event.target[0].value]);
      // clear text field
      event.target.reset();
    } else {
      // TODO toast saying duplicate
    }
  };

  /** Remove given value from state */
  const removeSelf = (value) => setValues(values.filter((v) => v !== value));

  const nameList = values.sort(/* TODO Add better sorter */).map((v) => (
    <li key={v} onClick={() => removeSelf(v)}>
      {v}
    </li>
  ));

  return (
    <div className="input-column">
      <form onSubmit={addValue}>
        <TextInput name={name} reference={nameInput} />
        <button type="submit">Add</button>
      </form>
      <ul>{nameList}</ul>
    </div>
  );
};

InputColumn.propTypes = {
  name: PropTypes.string.isRequired,
  values: PropTypes.array.isRequired,
  setValues: PropTypes.func.isRequired,
};

export default InputColumn;
