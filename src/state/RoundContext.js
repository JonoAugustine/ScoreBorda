import React from "react";
import { BordaState, Feature, Candidate } from "../api/models";

export const RoundContext = React.createContext(null);

export const RoundContextProvider = (props) => {
  const [features, setFeatures] = React.useState([
    "f0",
    "f1",
    "f2",
    "f3",
    "f4",
  ]);

  const [candidates, setCandidates] = React.useState([
    "c0",
    "c1",
    "c2",
    "c3",
    "c4",
  ]);

  const [state, setState] = React.useState(BordaState.SETUP.FEATURES);

  // TODO change name to iterator
  const [iterable, setIterable] = React.useState(null);

  /** Start a new round */
  const reset = () => {
    setFeatures([]);
    setCandidates([]);
    setState(BordaState.SETUP.FEATURES);
  };

  const addFeature = (featureName) => {
    setFeatures([...features, new Feature(featureName)]);
  };

  const addCandidate = (candidateName) => {
    setCandidates([...candidates, new Candidate(candidateName)]);
  };

  const removeFeature = (featureName) => {
    setFeatures(features.filter((n) => n !== featureName));
  };

  const removeCandidate = (candidateName) => {
    setCandidates(candidates.filter((n) => n !== candidateName));
  };

  const nextState = () => {
    const map = BordaState.flatMap();
    let index = map.indexOf(state);

    if (index < map.length) {
      setState(map[index + 1]);
    }

    return map[index + 1];
  };

  return (
    <RoundContext.Provider
      value={{
        features,
        setFeatures,
        addFeature,
        removeFeature,
        candidates,
        setCandidates,
        addCandidate,
        removeCandidate,
        state,
        nextState,
        reset,
        iterable,
        setIterable,
      }}
    >
      {props.children}
    </RoundContext.Provider>
  );
};
