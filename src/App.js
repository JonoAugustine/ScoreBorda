import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Home, Borda, History } from "./pages";

import "./index.scss";
import { RoundContextProvider } from "./state";
import { Navbar } from "./components";

function App() {
  return (
    <div className="app">
      <Router basename={process.env.PUBLIC_URL}>
        <Navbar />
        <Route path="/" exact component={Home} />
        <RoundContextProvider>
          <Route path="/borda" exact component={Borda} />
        </RoundContextProvider>
        <Route path="/history" exact component={History} />
      </Router>

      <footer>
        <a
          href="https://jonoaugustine.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Made By Jono
        </a>
      </footer>
    </div>
  );
}

export default App;
