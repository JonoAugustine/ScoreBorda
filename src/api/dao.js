import { Round } from "./models";

const LOCATION = {
  SCORES: "SCORES",
};

/** @returns {{features, candidates, state, time: Date}[]|null} */
export const loadScores = () => {
  const source = localStorage[LOCATION.SCORES];

  return source ? JSON.parse(source) : null;
};

export const saveScore = ({ features, candidates, state }) => {
  const loaded = loadScores();

  // If there is no save data, make an empty array and try again
  if (loaded === null) {
    localStorage[LOCATION.SCORES] = "[]";
    return saveScore({ features, candidates, state });
  }

  loaded.push({ features, candidates, state, time: Date.now() });

  localStorage[LOCATION.SCORES] = JSON.stringify(loaded);
};

export const clearScores = () => {
  localStorage[LOCATION.SCORES] = null;
};
