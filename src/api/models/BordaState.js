export default {
  SETUP: {
    FEATURES: "Features",
    CANDIDATES: "Candidates",
  },
  RUNNING: {
    CALIBRATION: "Calibration",
    SCORRING: "Scorring",
  },
  COMPLETE: "Complete",
  flatMap: function () { 
    return [
      this.SETUP.FEATURES,
      this.SETUP.CANDIDATES,
      this.RUNNING.CALIBRATION,
      this.RUNNING.SCORRING,
      this.COMPLETE
    ]
   }
};
