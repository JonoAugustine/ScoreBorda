/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
export const shuffleArray = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
};

export class BordaIterable {
  /**
   *
   * @param {*[]} array
   */
  constructor(array, shuffle = false) {
    this.array = array;
    /** The current static comparator  */
    this.left = 0;
    /** The current mobile comparator */
    this.right = 1;
    if (shuffle) shuffleArray(array);

    this.steps = 0;

    for (let i = 1; i <= array.length; i++) {
      this.steps += i * (array.length - i);
    }
  }

  /**
   * @returns the current comparing pair of values.
   */
  step = () => {
    const a = this.array[this.left];
    const b = this.array[this.right];

    // incriment right
    this.right++;

    // Check if right overflows
    if (this.right >= this.array.length) {
      // Incriment left and set right to left+1
      this.right = ++this.left + 1;
    }
    
    // Check if left overflows
    if (this.left === this.array.length) {
      // Return null, end of iteration
      return null;
    }

    // Return iteration pair
    return [a, b];
  };
}
